import React from 'react';
import {View, Text} from 'react-native';
import {styles} from '../styles/styles';

const ProfileScreen = () => {
  return (
    <View style={styles.root}>
      <Text>Settings Screen</Text>
    </View>
  );
};

ProfileScreen.options = {
  topBar: {
    title: {
      text: 'Profile',
    },
  },
};

export default ProfileScreen;
