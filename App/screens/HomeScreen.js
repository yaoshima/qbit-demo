import React, {createContext, useState} from 'react';

import PromoSlider from '../components/PromoSlider';
import ProductSlider from '../components/ProductSlider';
import {View, Text, StyleSheet} from 'react-native';
import Cart from '../components/Cart';

export const CartContext = createContext({
  cartCount: 0,
  setCartCount: () => {},
});

const HomeScreeen = () => {
  const [cartCount, setCartCount] = useState(1);

  return (
    <CartContext.Provider value={{cartCount, setCartCount}}>
      <Cart />

      <Text style={styles.text}>Nike App Store</Text>

      <PromoSlider />

      <ProductSlider />
    </CartContext.Provider>
  );
};

HomeScreeen.options = {
  topBar: {
    visible: false,
  },
};

const styles = StyleSheet.create({
  text: {
    paddingLeft: 20,
    paddingTop: 10,
    fontSize: 24,
    fontWeight: '600',
    color: '#3a3a3a',
  },
});

export default HomeScreeen;
