import React from 'react';
import {View, Text} from 'react-native';
import {styles} from '../styles/styles';
import Cart from '../components/Cart';

const FavoriteScreen = () => {
  return (
    <View style={styles.root}>
      <Text>Favorite Screen</Text>
    </View>
  );
};

FavoriteScreen.options = {
  topBar: {
    title: {
      text: 'Favorite',
    },
  },
};

export default FavoriteScreen;
