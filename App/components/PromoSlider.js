import React, {useState} from 'react';
import {
  View,
  Text,
  Image,
  ScrollView,
  useWindowDimensions,
  StyleSheet,
} from 'react-native';

const PromoSlider = () => {
  const width = useWindowDimensions().width;
  const height = width * 0.3;

  const [active, setActive] = useState(0);

  const images = [
    'https://media.endclothing.com/end-features/prodfeatures/ede7960b-ec5f-45ed-a89d-b59b120df6a5_END.+Best+Sneakers+of+the+Decade2%2C010.png?auto=compress,format',
    'https://media.endclothing.com/end-features/prodfeatures/dc08e72d-206f-47cc-b52f-d6f1fec566f6_END.+Best+Sneakers+of+the+Decade2%2C017.png?auto=compress,format',
    'https://media.endclothing.com/end-features/prodfeatures/03cb0fd2-b87d-4ed7-ad3f-f5623b5c5a57_END.+Best+Sneakers+of+the+Decade2%2C012.png?auto=compress,format',
  ];

  const change = ({nativeEvent}) => {
    const slide = Math.ceil(
      nativeEvent.contentOffset.x / nativeEvent.layoutMeasurement.width,
    );
    if (slide !== active) {
      setActive(slide);
    }
  };
  return (
    <View>
      <ScrollView
        pagingEnabled
        horizontal
        onScroll={change}
        showsHorizontalScrollIndicator={false}
        style={{width, height, marginVertical: 20}}>
        {images.map((image, index) => (
          <Image
            key={index}
            source={{uri: image}}
            style={[{width, height, resizeMode: 'cover'}, styles.image]}
          />
        ))}
      </ScrollView>
      <View style={styles.pagination}>
        {images.map((i, k) => (
          <Text key={k} style={k == active ? styles.activeDot : styles.dot}>
            •
          </Text>
        ))}
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  pagination: {
    flexDirection: 'row',
    position: 'absolute',
    bottom: -30,
    alignSelf: 'center',
  },
  dot: {
    color: '#888',
    fontSize: 50,
  },
  activeDot: {
    color: '#D3D3D3',
    fontSize: 50,
  },
  image: {
    borderRadius: 30,
    padding: 30,
  },
});

export default PromoSlider;
