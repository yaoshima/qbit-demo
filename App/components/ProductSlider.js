import React, {useState, useContext} from 'react';
import {
  View,
  Text,
  Image,
  ScrollView,
  useWindowDimensions,
  StyleSheet,
  Button,
  TouchableOpacity,
} from 'react-native';
import {CartContext} from '../screens/HomeScreen';
import ToastNative from './ToastNative';

const ProductSlider = () => {
  const {cartCount, setCartCount} = useContext(CartContext);

  const containerWidth = useWindowDimensions().width;
  const itemWidth = useWindowDimensions().width / 2.6;
  const height = itemWidth * 1.6;

  const [active, setActive] = useState(0);

  const images = [
    'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQnZzDTXW3TCsikObmvmSLCioJkWth5EV4Q_FTcWN5LC90u3R4nm_YA45VRkhSFg9UTs0kRONnl&usqp=CAchttps://media.endclothing.com/end-features/prodfeatures/ede7960b-ec5f-45ed-a89d-b59b120df6a5_END.+Best+Sneakers+of+the+Decade2%2C010.png?auto=compress,format',
    'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcS8neZgcLL9cKj1gSh4TKFH022E8H0ghji2rSuyFI52_JxE4XoVosbMKWnZmiVVu64CurbXezx3Mw&usqp=CAc',
    'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcRn-lNuw0473kecPRquo7sSNwOzyljdqFtylqy27FKqLEaerNMgbQTcrmROp06hA9nok5pNURiB&usqp=CAc',
    'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQ6SNdpLj7oh-EVT1pWhmMFgUqYdLtn2KudXnKlrN5uXGL5uclzDLySWCO8dFrMC9Yr1iqT3Mk&usqp=CAc',
  ];

  return (
    <View style={{marginVertical: 30}}>
      <ScrollView
        pagingEnabled
        horizontal
        showsHorizontalScrollIndicator={false}
        style={{width: containerWidth, height, marginVertical: 20}}>
        {images.map((image, index) => (
          <View key={index}>
            <Image
              source={{uri: image}}
              style={[
                {width: itemWidth, height, resizeMode: 'cover'},
                styles.image,
              ]}
            />
            <View
              style={{
                position: 'absolute',
                height: 40,
                width: itemWidth,
                backgroundColor: 'rgba(95,197,123,1)',
                bottom: 0,
                marginHorizontal: 10,
                borderBottomLeftRadius: 10,
                borderBottomRightRadius: 10,
              }}>
              <View style={styles.btnContainer}>
                <TouchableOpacity
                  onPress={() => {
                    setCartCount((count) => count + 1);
                    ToastNative.show('Item added', ToastNative.SHORT);
                  }}
                  style={{backgroundColor: '#FFF'}}>
                  <Image
                    source={require('../assets/images/add.png')}
                    style={{width: 30, height: 30}}
                  />
                </TouchableOpacity>
              </View>
            </View>
          </View>
        ))}
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  image: {
    borderRadius: 30,
    margin: 10,
  },
  btnContainer: {
    marginLeft: 10,
    marginVertical: 5,
    width: 30,
  },
});

export default ProductSlider;
