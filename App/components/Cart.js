import React, {useContext} from 'react';
import {StyleSheet, View, Text, Image} from 'react-native';
import {CartContext} from '../screens/HomeScreen';

const Cart = () => {
  const {cartCount, setCartCount} = useContext(CartContext);
  return (
    <View style={[{padding: 5}, styles.iconContainer]}>
      <View
        style={{
          position: 'absolute',
          height: 30,
          width: 30,
          borderRadius: 15,
          backgroundColor: 'rgba(95,197,123,0.8)',
          right: 15,
          bottom: 15,
          alignItems: 'center',
          justifyContent: 'center',
          zIndex: 2000,
        }}>
        <Text style={{color: 'white', fontWeight: 'bold'}}>{cartCount}</Text>
      </View>
      <Image
        style={{alignSelf: 'flex-end'}}
        source={require('../assets/images/cart.png')}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  iconContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 20,
    paddingLeft: 20,
    paddingTop: 10,
    marginRight: 5,
  },
});

export default Cart;
