/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */
const {Navigation} = require('react-native-navigation');
import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  Button,
} from 'react-native';

import ProfileScreen from './App/screens/ProfileScreen';
import HomeScreen from './App/screens/HomeScreen';
import FavoriteScreen from './App/screens/FavoriteScreen';

const App: () => React$Node = (props) => <HomeScreen />;

Navigation.registerComponent('HomeScreen', () => HomeScreen);
Navigation.registerComponent('ProfileScreen', () => ProfileScreen);
Navigation.registerComponent('FavoriteScreen', () => FavoriteScreen);

Navigation.setDefaultOptions({
  statusBar: {
    backgroundColor: '#4d089a',
  },
  topBar: {
    title: {
      color: 'white',
    },
    backButton: {
      color: 'white',
    },
    background: {
      color: '#4d089a',
    },
  },
});

Navigation.events().registerAppLaunchedListener(async () => {
  Navigation.setRoot({
    root: {
      bottomTabs: {
        id: 'BOTTOM_TABS_LAYOUT',
        children: [
          {
            stack: {
              id: 'HOME_TAB',
              children: [
                {
                  component: {
                    id: 'HOME_SCREEN',
                    name: 'HomeScreen',
                  },
                },
              ],
              options: {
                bottomTab: {
                  icon: require('./App/assets/images/home.png'),
                },
              },
            },
          },
          {
            stack: {
              id: 'FAVORITE_TAB',
              children: [
                {
                  component: {
                    id: 'FAVORITE_SCREEN',
                    name: 'FavoriteScreen',
                  },
                },
              ],
              options: {
                bottomTab: {
                  icon: require('./App/assets/images/fav.png'),
                },
              },
            },
          },
          {
            stack: {
              id: 'PROFILE_TAB',
              children: [
                {
                  component: {
                    id: 'PROFILE_SCREEN',
                    name: 'ProfileScreen',
                  },
                },
              ],
              options: {
                bottomTab: {
                  icon: require('./App/assets/images/profile.png'),
                },
              },
            },
          },
        ],
      },
    },
  });
});

export default App;
